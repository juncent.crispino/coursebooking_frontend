import {Container} from 'react-bootstrap'
import { useState, useEffect, useContext } from 'react'
import UserView from '../components/UserView'
import UserContext from '../UserContext'
import AdminView from '../components/AdminView'

export default function Courses(){
	
	const {user} = useContext(UserContext)
	const [courses, setCourses] = useState([])

	const fetchData = () => {
		fetch(`${process.env.REACT_APP_API_URL}/courses/all`)
		.then(res => res.json())
		.then(data => {
			// console.log(data)
			//this is an example of state (when data is passed on a component)
			/*const coursesArr = data.map(course => {
				// console.log(course)
				return(
					//"courseData" in this case a user-defined label for the data we want to pass to our Course component
					//"key" is not user-defined; React specifically nees a unique key prop for each dynamically generated component
					<Course courseProp={course} key={course._id}/>
				)
			})*/
			//set the courses state to the result of our map function to bring our returned couse componenets outside of the scope of this .then where our return statement below can see
			/*setCourses(coursesArr)*/
			setCourses(data)
		})
	}

	useEffect(() => {
		fetchData()
	}, [])

	

	return(
		<Container>
		{(user.isAdmin === true) 
			? <AdminView coursesData={courses} fetchData={fetchData}/>
			: <UserView coursesData={courses}/>
		}
		</Container>
	)
}