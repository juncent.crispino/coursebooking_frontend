import {useEffect, useState} from 'react'
import {Container, Table} from 'react-bootstrap'
import { Link } from 'react-router-dom'
import { ImArrowRight2 } from 'react-icons/im';

export default function UserProfile(){

	const [enrollments, setEnrollments] = useState([])
	const [courses, setCourses] = useState([])
	const [user, setUser] = useState({})
	const [studentCourses, setStudentCourses] = useState([])
	const { firstName, lastName, email, mobileNo} = user

	useEffect(()=>{
		fetch(`${process.env.REACT_APP_API_URL}/users/details`, {
			headers: {
				Authorization: `Bearer ${localStorage.getItem('token')}`
			}
		})
		.then(res => res.json())
		.then(data => {
			setEnrollments(data.enrollments)
			setUser(data)
		})

		fetch(`${process.env.REACT_APP_API_URL}/courses/all`)
		.then(res => res.json())
		.then(data => {		
				setCourses(data)		
		})
	},[])

	useEffect(()=>{
		const enrollmentsArr = enrollments.map(enrolledcourse => {
			console.log(enrolledcourse)
			return(
				<tbody key={enrolledcourse.courseId}>
					{courses.map(course => {
						return(
							<tr key={course._id}>
								{enrolledcourse.courseId === course._id
								?
								<>
								<td>{course.name}</td>
								<td>{course.schedule}</td>
								<td>{enrolledcourse.status}</td>
								<td>{new Date(enrolledcourse.enrolledOn).toLocaleDateString('en-Us')}</td>
								<td><Link className="btn coursebtn" to={`/courses/${course._id}`}><b> <ImArrowRight2/></b></Link></td>
								</>
								: null
								}
							</tr>
						)
					})}
				</tbody>
			)
		})
		setStudentCourses(enrollmentsArr)

	},[enrollments, courses])

	return (
		<>
			<Container>
				<div className="userinfo">
					<div>
						<b>STUDENT INFORMATION</b>
					</div>
					<hr/>
					<div>
						<span>First Name : </span><span><b>{firstName}</b></span>
					</div>
					<div>
						<span>Last Name : </span><span><b>{lastName}</b></span>
					</div>
					<div>
						<span>Email Address : </span><span><b>{email}</b></span>
					</div>
					<div>
						<span>Mobile Number : </span><span><b>{mobileNo}</b></span>
					</div>
				</div>
			</Container>
			<Container>
				<div className="userinfo">
					<div className="mb-3">
						<b>ENROLLED COURSES</b>
					</div>
					<Table striped bordered responsive>
						<thead>
							<tr>
								<th>Name</th>
								<th>Schedule</th>
								<th>Status</th>
								<th>Enrolled On</th>
								<th>Details</th>
							</tr>
						</thead>
						{studentCourses}
					</Table>
					
				</div>
				
			</Container>
		</>
	)
}