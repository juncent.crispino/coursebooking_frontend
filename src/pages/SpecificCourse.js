import { useContext, useEffect, useState } from 'react';
import { Container} from 'react-bootstrap'
import UserContext from '../UserContext'
import { Link, useParams, useHistory } from 'react-router-dom'
import Swal from 'sweetalert2'


export default function SpecificCourse(){

	const [course, setCourse] = useState({})
	const [price, setPrice] = useState('')
	const { name, description, instructor, schedule, availableSlots } = course

	const history = useHistory()
	console.log(availableSlots)
	// console.log(course)
	const { user } = useContext(UserContext)
	// console.log(user)
	//useParams() contains any values we are trying to pass in the URL stored in a key/value pair
	const { courseId } = useParams();

	useEffect(()=> {
		// console.log(courseId)
		fetch(`${process.env.REACT_APP_API_URL}/courses/${courseId}`)
		.then(res => res.json())
		.then(data => {
			console.log(data)
			setCourse(data)
			setPrice(data.price)
		})
	}, [courseId])

	function enrollUser(e){
		e.preventDefault()
		fetch(`${process.env.REACT_APP_API_URL}/users/enroll `, {
			method: 'POST',
			headers: {
				'Content-Type': 'application/json',
				Authorization: `Bearer ${localStorage.getItem('token')}`
			},
			body: JSON.stringify({
				userId: user.id,
				courseId: courseId,
				availableSlots: availableSlots
			})
		})
		.then(res => res.json())
		.then(data => {
			if(data === true){
				Swal.fire({
					title: "Successfully Enrolled",
					icon: "success",
					text: `You have successfully enrolled to ${course.name}`
				})
				history.push('/courses')
			}else{
				Swal.fire({
					title: "Sorry",
					icon: "error",
					text: "Something went wrong, Well come back to you soon."
				})
			}
		})

	}     
	 return(
	 	<Container className="d-flex justify-content-center">
	 		<div className="specific_card">
	 			<div className="specific_card_header">
	 				{name}
	 			</div>
	 			<div className="specific_card_body row d-flex">
	 				<div className="col-md-7">
	 					<div className="specific_card_description">{description}</div>
	 				</div>
	 				<div className="col-md-5 specific_card_content">
 						<div className="mb-3">Instructor : <b>{instructor}</b></div>
 						<div className="mb-3">Schedule : <b>{schedule}</b></div>
 						<div className="mb-3">Availble Slots : <b>{availableSlots}</b></div>
 						<div className="">Price : <b>₱{price.toLocaleString()}</b></div>	
	 				</div>	
	 			</div>
	 			<div>
	 				{user.id !== null
	 					?<div className="d-flex mx-4 justify-content-between">
	 						<Link className="btn coursebtn" to="/courses"><b>Back</b></Link>
	 						<div className="btn coursebtn" onClick={(e) => enrollUser(e)}><b>Enroll</b></div>
	 					</div>
	 					:
	 					<div className="d-flex justify-content-end mr-3">
	 						<Link className="btn btn-danger" to="/login" >Log in to to Enroll</Link>
	 					</div>
	 				}
	 			</div>
	 		</div>
	 	</Container>
	 )
}


