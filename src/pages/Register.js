import React, { useState, useEffect, useContext } from 'react';
import { Form, Container } from 'react-bootstrap'
import { Redirect, useHistory } from 'react-router-dom'
import UserContext from '../UserContext'
import Swal from 'sweetalert2'

export default function Register() {


	const [email, setEmail] = useState('')
	const [password, setPassword] = useState('')
	const [verifyPassword, setVerifyPassword] = useState('')
	const [firstName, setFirstName] = useState('')
	const [lastName, setLastName] = useState('')
	const [mobileNo, setMobileNo] = useState('')
	const {user} = useContext(UserContext)
	const history = useHistory()

	//let's declare a variable that will describe the state of the register button component

	const[registerButton, setRegisterButton] = useState(false)

	//EXPLANATION FOR THE SCRIPT WE USE IN THE REGISTRATION
	//The values is in the fields of the form is bound to the getter of the state and the event is bound to the setter. This is what is called two-way binding.

	//"Bind" the user input states into their corresponding input fields via onChange event handler. Also, set the values of the form input fields as their respiective states to implement two-way binding.

	//What is two way binding?
	//The two data binding means the data we changed in the view has updated the state.
	//The data in the state has updated view

	//let's create a function that will simulate an actual register page 
	//we want to add an alert if registration is successful

	function registerUser(e){
		e.preventDefault()

		fetch(`${ process.env.REACT_APP_API_URL }/users/checkEmail `, {
			method: 'POST',
			headers: {
				'Content-Type': 'application/json'
			},
			body: JSON.stringify({
				email: email
			})
		})
		.then(res => res.json())
		.then(data => {
			console.log(data)
			if(data === false){
				inputUserDetails()
				history.push('/login')

			}else{
				return Swal.fire({
						title: 'Email already exist',
						icon: 'error',
						text: 'Please use other email'
					})
			}
		})


		
		//clearout the data inside the input fields
		//call the state setters
		// setEmail('')
		// setPassword('')
		// setVerifyPassword('')
	}

	const inputUserDetails = () => {
		fetch(` ${ process.env.REACT_APP_API_URL }/users/register `, {
			method: 'POST',
			headers: {
				'Content-Type': 'application/json'
			},
			body: JSON.stringify({
				firstName: firstName,
				lastName: lastName,
				email: email,
				mobileNo: mobileNo,
				password: password,
			})
		})
		.then(res => res.json())
		.then(data => {
			if(data === true){
			 return	Swal.fire({
						title: 'Yaaaaaaay!!!',
						icon: 'success',
						text: 'You have successfully register'
					})
			}else{
				return Swal.fire({
						title: 'Sorry',
						icon: 'error',
						text: 'Check your register details and try again'
					})
			}
		})
	}


	//When should I use useEffect?
	//useEffect() is for side-effects only

	//if the functional components makes calculations that don't target the output value, then calculations are named side-effects. Example of side-effect are fetch requests, manipulating state changes/updates.
	useEffect(() => {
		//let's modify our current logic to validate the data inserted by the user

		if((email !== '' && password !=='' && verifyPassword !=='') && (password === verifyPassword) && mobileNo.length === 11){
			setRegisterButton(true)
		}else{
			setRegisterButton(false)
		}

	}, [email, password, verifyPassword, mobileNo])
/*	

	Swal.fire({
		title: 'Oooops!!!',
		icon: 'error',
		text: 'Something went wrong'
	})*/

	console.log(typeof mobileNo)
	console.log(mobileNo.length)

	if(user.id != null){
		return <Redirect to="/"/>

	}
	return(
		<Container style={{ maxWidth: 600 }}>
			<Form className="mt-3" onSubmit={(e) => registerUser(e)}>

			<Form.Group>
				<Form.Label>First Name</Form.Label>
				<Form.Control type="text" placeholder="Enter First Name" value={firstName}  onChange={e => setFirstName(e.target.value)} required/>
			</Form.Group>				
			<Form.Group>
				<Form.Label>Last Name</Form.Label>
				<Form.Control type="text" placeholder="Enter Last Name" value={lastName}  onChange={e => setLastName(e.target.value)} required/>
			</Form.Group>
				<Form.Group>
					<Form.Label>Email Address</Form.Label>
					<Form.Control type="email" placeholder="Enter email" value={email} onChange={e => setEmail(e.target.value)} required/>
					<Form.Text className="text-muted">
						We'll Never share your email with anyone else
					</Form.Text>
				</Form.Group>
				<Form.Group>
					<Form.Label>Mobile Number</Form.Label>
					<Form.Control type="number" placeholder="Enter Mobile Number" value={mobileNo}  onChange={e => setMobileNo(e.target.value)} required/>
				</Form.Group>
				<Form.Group>
					<Form.Label>Password</Form.Label>
					<Form.Control type="password" placeholder="Enter Password" value={password} onChange={e => setPassword(e.target.value)} required/>
				</Form.Group>
				<Form.Group>
					<Form.Label>Verify Password:</Form.Label>
					<Form.Control type="password" placeholder="Verify Password" value={verifyPassword}  onChange={e => setVerifyPassword(e.target.value)} required/>
				</Form.Group>
				{registerButton ?
  
					<button className="btn subbtn" type="submit"><b>Submit</b></button>
				:

					<button className="btn subbtn" type="submit" disabled>Submit</button>
				}	
			</Form>
		</Container>
	)
}