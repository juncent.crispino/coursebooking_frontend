import {Row, Col} from 'react-bootstrap'

export default function Highlights(){
	return(
		<Row>
			<Col xs={12} md={4}>
				<div className="card-highlight">
					<div>
						<h5>Learn From Home</h5>
						<hr className="hr-highlight"/>
						<div>
							Lorem ipsum dolor sit amet consectetur adipisicing elit. Nam consequuntur ea, animi, rem voluptate voluptas provident quasi alias laborum optio ad soluta molestiae. Non mollitia praesentium distinctio? Ab, quaerat ea.
						</div>
					</div>
				</div>
			</Col>
			<Col xs={12} md={4}>
				<div className="card-highlight">
					<div>
						<h5>Study Now Pay Later</h5>
						<hr className="hr-highlight"/>
						<div>
							Lorem ipsum dolor sit amet consectetur adipisicing elit. Nam consequuntur ea, animi, rem voluptate voluptas provident quasi alias laborum optio ad soluta molestiae. Non mollitia praesentium distinctio? Ab, quaerat ea.
						</div>
					</div>
				</div>
			</Col>
			<Col xs={12} md={4}>
				<div className="card-highlight">
					<div>
						<h5>Be a Parf of Our Community</h5>
						<hr className="hr-highlight"/>
						<div>
							Lorem ipsum dolor sit amet consectetur adipisicing elit. Nam consequuntur ea, animi, rem voluptate voluptas provident quasi alias laborum optio ad soluta molestiae. Non mollitia praesentium distinctio? Ab, quaerat ea.
						</div>
					</div>
				</div>
			</Col>
		</Row>

	)
}