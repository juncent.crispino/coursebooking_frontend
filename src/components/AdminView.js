import { useState, useEffect } from 'react'
import { Table, Button, Modal, Form } from 'react-bootstrap'
import Swal from 'sweetalert2'

export default function AdminView(props){

	//destructure our courses data from the props being passed by th parent component
	//ass well as our fetchData function
	
	const { coursesData, fetchData } = props
	const [ courses, setCourses] = useState([])
	const [ showEdit, setShowEdit ] = useState(false)
	const [ showAdd, setShowAdd ] = useState(false)
	const [name, setName] = useState("")
	const [description, setDescription] = useState("")
	const [price, setPrice] = useState("")
	const [instructor, setInstructor] = useState("")
	const [schedule, setSchedule] = useState("")
	const [availableSlots, setAvailableSlots] = useState("")
	const [id, setId] = useState("")

	const openAdd = () => setShowAdd(true)
	const closeAdd = () => setShowAdd(false)
	

	const openEdit = (courseId) => {
		// console.log(courseId)
		fetch(`${process.env.REACT_APP_API_URL}/courses/${courseId}`)
		.then(res => res.json())
		.then(data => {
			// console.log(data)
			setName(data.name)
			setDescription(data.description)
			setInstructor(data.instructor)
			setSchedule(data.schedule)
			setAvailableSlots(data.availableSlots)
			setPrice(data.price)
			setId(data._id)

		})
		setShowEdit(true)
	}

	const closeEdit = () => {
		setShowEdit(false)
		setName("")
		setDescription("")
		setInstructor("")
		setSchedule("")
		setAvailableSlots("")
		setPrice("")
	}

	// console.log(courses)
	// console.log(props)
	useEffect(() => {

	const archiveToggle = (courseId, isActive) => {
		fetch(`${process.env.REACT_APP_API_URL}/courses/${courseId}/archive`,{
			method: 'PUT',
			headers: {
				"Content-Type":"application/json",
				Authorization: `Bearer ${localStorage.getItem('token')}`
			},
			body: JSON.stringify({
				isActive: isActive
			})
		})
		.then(res => res.json())
		.then(data => {
			//call fetchData upon receiving the response from the server so we can repopulate the data, which makes our component re-mount
			
			// console.log(data)
			if(data === true){
				fetchData()
				return Swal.fire({
					title: 'Success',
					icon: 'success',
					text: `Course successfully archive/unarchived.`
				})
			}else{
				fetchData()
				return Swal.fire({
					title: 'Error',
					icon: 'error',
					text: `Something went wrong.`
				})
			}
		})

	}	
		const coursesArr = coursesData.map(course => {
			return(
				<tr key={course._id}>
					<td>{course.name}</td>
					<td>{course.description}</td>
					<td>{course.instructor}</td>
					<td>{course.schedule}</td>
					<td>{course.availableSlots}</td>
					<td>{course.price}</td>
					<td>
							{course.isActive
								?<span>Available</span>
								:<span>Unavailable</span>
							}
					</td>
					<td>
							<Button variant="primary" size="sm" onClick={()=> openEdit(course._id)} >Update</Button>
							{course.isActive
								?
								<Button variant="danger" size="sm" onClick={() => archiveToggle(course._id, course.isActive)}>Disable</Button>
								:
								<Button variant="success" size="sm" onClick={() => archiveToggle(course._id, course.isActive)}>Enable</Button>

							}
					</td>
				</tr>
			)
		})

		setCourses(coursesArr)

	}, [coursesData, fetchData])

	const addCourse = (e) => {
		e.preventDefault()
		fetch(` ${ process.env.REACT_APP_API_URL}/courses `, {
			method: 'POST',
			headers: {
				Authorization: `Bearer ${ localStorage.getItem('token')}`,
				"Content-Type": "application/json"
			},
			body: JSON.stringify({
				name: name,
				description: description,
				instructor: instructor,
				schedule: schedule,
				availableSlots: availableSlots,
				price: price
			})
		})
		.then(res => res.json())
		.then(data => {
			if(data === true){
				fetchData()
				Swal.fire({
					title: 'Success',
					icon: 'success',
					text: `You have successfully added ${name}`
				})
				closeAdd()
				setName("")
				setDescription("")
				setInstructor("")
				setSchedule("")
				setPrice("")
				setAvailableSlots("")
				
			}else{
				fetchData()
				Swal.fire({
					title: 'Sorry',
					icon: 'error',
					text: 'Something went wrong'
				})

			}
		})
	}

	const editCourse = (e) => {
		e.preventDefault()
		//Optional homework
		//Finish the edit course functionality (when user submits form, the course is edited based on the input values)
		console.log("hello")
		fetch(` ${process.env.REACT_APP_API_URL}/courses/updateCourse/${id}`, {
			method: 'PUT',
			headers: {
				"Content-Type":"application/json",
				Authorization: `Bearer ${localStorage.getItem('token')}`
			},
			body: JSON.stringify({
				name: name,
				description: description,
				instructor: instructor,
				schedule: schedule,
				availableSlots: availableSlots,
				price: price
			})
		})
		.then(res => res.json())
		.then(data => {
			
			// console.log(data)
			if(data === true){
				fetchData()
				Swal.fire({
					title: 'Success',
					icon: 'success',
					text: `You have successfully updated ${name}`
				})
				closeEdit()
			}else{
				Swal.fire({
					title: 'Sorry',
					icon: 'error',
					text: 'Something went wrong'
				})
			}
		})
		closeEdit()

	}
	return(
		<>
		<div className="text-center my-4">
			<h2>Admin DashBoard</h2>
			<div className="d-flex justify-content-center">
				<Button variant="primary" onClick={openAdd}>Add New Course</Button>
			</div>
		</div>
		<Table striped bordered hover responsive>
			<thead className="bg-dark text-white text-center">
				<tr>
					<th>Name</th>
					<th>Description</th>
					<th>Instructor</th>
					<th>Schedule</th>
					<th>Available Slots</th>
					<th>Price</th>
					<th>Availability</th>
					<th>Actions</th>
				</tr>
			</thead>
			<tbody className="text-center text-justified" >
				{courses}
			</tbody>
		</Table>
		{/*EDIT MODAL*/}
		<Modal show={showEdit} onHide={closeEdit}>
			<Form onSubmit={(e) => editCourse(e)}>
				<Modal.Header closeButton>
					<Modal.Title>Edit Course</Modal.Title>
				</Modal.Header>
				<Modal.Body>
					<Form.Group controlId="courseName">
						<Form.Label>Name</Form.Label>
						<Form.Control type="string" value={name} onChange={e => setName(e.target.value)} required/>
					</Form.Group>
					<Form.Group controlId="courseDescription">
						<Form.Label>Description</Form.Label>
						<Form.Control type="string" value={description} onChange={e => setDescription(e.target.value)}required/>
					</Form.Group>
					<Form.Group controlId="courseDescription">
						<Form.Label>Instructor</Form.Label>
						<Form.Control type="string" value={instructor} onChange={e => setInstructor(e.target.value)}required/>
					</Form.Group>
					<Form.Group controlId="courseDescription">
						<Form.Label>Schedule</Form.Label>
						<Form.Control type="string" value={schedule} onChange={e => setSchedule(e.target.value)}required/>
					</Form.Group>
					<Form.Group controlId="courePrice">
						<Form.Label>Price</Form.Label>
						<Form.Control type="number" value={price} onChange={e => setPrice(e.target.value)} required/>
					</Form.Group>
					<Form.Group controlId="coureAvailableSlots">
						<Form.Label>Availble Slots</Form.Label>
						<Form.Control type="number" value={availableSlots} onChange={e => setAvailableSlots(e.target.value)} required/>
					</Form.Group>
				</Modal.Body>
				<Modal.Footer>
					<Button variant="secondary" onClick={closeEdit}>Cancel</Button>
					<Button variant="success" type="submit">Submit</Button>
				</Modal.Footer>
			</Form>
		</Modal>
		{/*ADD MODAL*/}
		<Modal show={showAdd} onHide={closeAdd}>
			<Form onSubmit={(e) => addCourse(e)}>
				<Modal.Header closeButton>
					<Modal.Title>Add Course</Modal.Title>
				</Modal.Header>
				<Modal.Body>
					<Form.Group controlId="courseName">
						<Form.Label>Name</Form.Label>
						<Form.Control type="" value={name} onChange={e => setName(e.target.value)} required/>
					</Form.Group>
					<Form.Group controlId="courseDescription">
						<Form.Label>Description</Form.Label>
						<Form.Control type="" value={description} onChange={e => setDescription(e.target.value)}required/>
					</Form.Group>
					<Form.Group controlId="courseDescription">
						<Form.Label>Instructor</Form.Label>
						<Form.Control type="string" value={instructor} onChange={e => setInstructor(e.target.value)}required/>
					</Form.Group>
					<Form.Group controlId="courseDescription">
						<Form.Label>Schedule</Form.Label>
						<Form.Control type="string" value={schedule} onChange={e => setSchedule(e.target.value)}required/>
					</Form.Group>
					<Form.Group controlId="courePrice">
						<Form.Label>Price</Form.Label>
						<Form.Control type="" value={price} onChange={e => setPrice(e.target.value)} required/>
					</Form.Group>
					<Form.Group controlId="coureAvailableSlots">
						<Form.Label>Availble Slots</Form.Label>
						<Form.Control type="number" value={availableSlots} onChange={e => setAvailableSlots(e.target.value)} required/>
					</Form.Group>
				</Modal.Body>
				<Modal.Footer>
					<Button variant="secondary" onClick={closeAdd}>Cancel</Button>
					<Button variant="success" type="submit">Submit</Button>
				</Modal.Footer>
			</Form>
		</Modal>
		</>
	)
}