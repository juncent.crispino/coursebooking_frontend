import { useState, useEffect } from 'react'
import Course from './Course'

export default function UserView({coursesData}){

	const [courses, setCourses] = useState([])

	useEffect(() => {
		const coursesArr = coursesData.map(course => {
			if(course.isActive === true){
				return(
					<div className="ml-3" key={course._id}>
						<Course courseProp={course} key={course._id}/>
					</div>
				)
			}else{
				return null
			}
		})
		setCourses(coursesArr)
	}, [coursesData])

	return(
		<div className="containerBody">
			{courses}
		</div>
	)
}