import { Card } from 'react-bootstrap'

import { Link } from 'react-router-dom'

import { ImArrowRight2 } from 'react-icons/im';

export default function Course({courseProp}){

	const { _id, name, description, price, instructor, schedule, availableSlots } = courseProp

	/*console.log(name)*/

	//Course states 

	//[getter, setter] -> state hooks

	// getter and setter identifiers are user defined

	//setter(<expression>) -> is a function that accepts an expression as an argument

	//the argument provided will update the value of getter 

	//useState(<initial_value>), initial value can be any valid JavaScript data type

	// const [seatsCount, setSeatsCount] = useState(30);

	// const [count, setCount] = useState(0); //will monitor enrollment in a course

	// console.log(count);

	//for the enroll button

	// const [isDisabled, setIsDisabled] = useState(false)

/*	function enroll(){

		if(seat < 1){

			alert("No more seats")

		}else{

			seatCount(seat - 1);

			setCount(count + 1);	

			console.log(count);

			console.log(seat);
		}
	}*/

/*	function enroll(){

			setSeatsCount(seatsCount - 1);

			setCount(count + 1);	

			console.log(count);

			console.log(seatsCount);
	}


	useEffect(() => {
		if(seatsCount === 0){
			setIsDisabled(true)
		}
	}, [seatsCount])*/
	/*

	Sir Solution 1:

	const [seatsCount, setSeatsCount] = useState(30)

	function enroll(){

		if(seatsCount === 0){
	
			alert("No more seats availbale")

		}else{
			
			seatCount(seat - 1);

			setCount(count + 1);	
	
		}		

	}

	*/

	return(
		<>
			<Card className="card">
			<Card.Header className="text-center"><b>{name.toUpperCase()}</b></Card.Header>
				<Card.Body>
					<div>{description.slice(0, 150)} . . .</div>
					<div className="py-2"></div>
					<h6>Instructor : <b>{instructor}</b></h6>
					<h6>Schedule : <b>{schedule}</b></h6>
					<h6>Availble Slots : <b>{availableSlots}</b></h6>
					<h6>Price : <b>₱ {price.toLocaleString()}</b></h6>
					
					
					
					{/*<p>{count} enrollees</p>
					<p>{seatsCount} Seat Remaining</p>
					{isDisabled ? 
						<Button onClick={enroll} variant="primary" disabled>Enroll</Button>
					:
						<Button onClick={enroll} variant="primary" >Enroll</Button>

					}*/}
					
				</Card.Body>
				<div className="d-flex justify-content-end align-items-center py-2 pr-2 border-top">
					<Link className="btn coursebtn" to={`/courses/${_id}`}><b>Details <ImArrowRight2/></b></Link>
				</div>		
			</Card>
		</>

		

	)
}